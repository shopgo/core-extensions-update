At a certain period of time, all ShopGo clients have had an issue with Aramex extension and/or some ordering functionalities at the admin panel.
These issues will be solved by updating the following 4 extensions:
shopgo-aramex
shipping-core
shopgo-core
advanced-ifconfig
It took us some time to update those 4 extensions via Modgit (as no composer was used at that time, and the Modman was not yet public at our stores), and the admin panel will go down if Aramex extension is the only updated one.
Another thing to mention that the update requires to remove core extensions totally and install them from Gitub -because they were moved to github-.

A bash script that will recognize the repository URL and catch it's Modgit name to do removing and adding functionalities.

We added Skynet to the list later, as an addition to the store extensions.